// app.js

const express = require("express");
const connectDB = require("./config/db");
var cors = require("cors");
var bodyParser = require("body-parser");
const cookieParser = require("cookie-parser");

var multer = require("multer");
const path = require("path");

// routes
const posts = require("./routes/api/posts");

const pages = require("./routes/api/pages");

const tours = require("./routes/api/tours");

const app = express();

require("./routes/api/multerImpl")(app);

const mongoose = require("mongoose");

const passport = require("passport");

const users = require("./routes/api/users");

// Connect Database
connectDB();

// cors
//to not get any deprecation warning or error
//support parsing of application/x-www-form-urlencoded post data
app.use(bodyParser.urlencoded({ extended: true }));

//to get json data
// support parsing of application/json type post data
app.use(bodyParser.json());
app.use(
  bodyParser.urlencoded({
    extended: false,
  })
);
app.use(cookieParser());

app.use(cors());

app.use("/public", express.static("public"));

// Init Middleware
app.use(express.json({ extended: false }));

app.get("/", (req, res) => res.send("Hello world!"));

// Passport middleware
app.use(passport.initialize());
// Passport config
require("./config/passport")(passport);
// Routes
app.use("/api/users", users);

//https://stackoverflow.com/questions/48914987/send-image-path-from-node-js-express-server-to-react-client
app.use("/uploads", express.static("uploads"));

// use Routes
app.use("/api/posts", posts);
app.use("/api/pages", pages);
app.use("/api/tours", tours);

app.use("/api/customers", require("./routes/users"));

const port = process.env.PORT || 8082;

app.listen(port, () => console.log(`Server running on port ${port}`));
